
/* Плавно по якорям */
$("a.scroll-to").on("click", function(e){
  e.preventDefault();
  var anchor = $(this).attr('href');
  $('html, body').stop().animate({
      scrollTop: $(anchor).offset().top - 60
  }, 800);
  $('#menu').toggleClass('active')
});

/* Toogle Burger */
$('#burger').on('click', function () {
  $('#menu').toggleClass('active')
});
$('#menu-cross').on('click', function () {
  $('#menu').toggleClass('active')
});

/* Close Search */
$('#search_m').on('click', function (e) {
  if ($('.form__search_m').hasClass('active')) {
  }
  else {
    $('.form__search_m').toggleClass('active');
    e.preventDefault();
  }
});
$('#form-cross').on('click', function () {
  $('.form__search_m').toggleClass('active');
});
const formSearch = $('.form__search_m');
/* $(document).click(function (e) {
  if (!formSearch.is(e.target) && formSearch.has(e.target).length === 0) {
    formSearch.removeClass('active');
  };
}); */

/* Dropdown */
const dropBtn = $('.dropdown__btn'),
      dropContainer = $('.dropdown__container');

dropBtn.on('click', function () {
  if ($(this).hasClass('active')) {
    $(this).removeClass('active');
  } else {
    $(dropBtn).removeClass('active');
    $(this).addClass('active');
  }
});

$(document).click(function (e) {
  if (!dropBtn.is(e.target) && !dropContainer.is(e.target) && dropContainer.has(e.target).length === 0) {
    dropBtn.removeClass('active');
  };
  if (!formSearch.is(e.target) && formSearch.has(e.target).length === 0) {
    formSearch.removeClass('active');
  };
  if (!formSearch.is(e.target) && formSearch.has(e.target).length === 0) {
    formSearch.removeClass('active');
  };
});

/* Скрытие меню при скроле */
let prevScroll
let lastShowPos

$(window).on("scroll", function () {
  const scrolled = $(window).scrollTop()

  if (scrolled > 500 && scrolled > prevScroll) {
    $(".menu").removeClass("active")
    lastShowPos = scrolled
  }
  prevScroll = scrolled
})

/* Sweper */
const heroSwiper = new Swiper('.hero__swiper', {
  speed: 1000,
  slidesPerView: 1,
  slidesPerColumn: 1,
  loop: true,
  autoplay: {
    delay: 3000,
  },
});
const gallerySwiper = new Swiper('.gallery__swiper', {
  slidesPerView: 1,
  loop: false,
  breakpoints: {
    475: {
      slidesPerView: 2,
      slidesPerColumn: 2,
      slidesPerGroup: 2,
      spaceBetween: 34,
    },
    1360: {
      slidesPerView: 3,
      slidesPerColumn: 2,
      slidesPerGroup: 3,
      spaceBetween: 50,
    },
  },
  pagination: {
    el: '.swiper-pagination',
    type: 'custom',
    renderCustom: function (gallerySwiper, current, total) {
      return current + ' / ' + total;
    },
  },
  navigation: {
    nextEl: '.gallery__swiper .swiper-button-next',
    prevEl: '.gallery__swiper .swiper-button-prev',
  },
});



/* Choices */
const autorGallery = document.querySelector('#autor');
const autorChoices = new Choices(autorGallery, {
  itemSelectText: '',
  searchEnabled: false,
});

const directionGallery = document.querySelector('#direction');
const directionChoices = new Choices(directionGallery, {
  itemSelectText: '',
  searchEnabled: false,
});

const techniqueGallery = document.querySelector('#technique');
const techniqueChoices = new Choices(techniqueGallery, {
  itemSelectText: '',
  searchEnabled: false,
});

/* $(".gallery__swiper-slide .swiper-item").on('click', function() {
  $(".popup").addClass('active');
  $(this).clone().appendTo('.popup__container');
  $('+ .swiper-slide__descr', this).clone().appendTo('.popup__container');
  $('.popup__container .swiper-slide__descr').show();
  $('body').css('overflow', 'hidden');
});
$(".popup__bg,  .popup__container > .cross").click(function(){
  $(".popup").removeClass('active');
  $('.popup__container > .swiper-item, .popup__container > .swiper-slide__descr').remove();
  $('body').css('overflow', '');
}); */

$('.gallery__swiper-wrapper').lightGallery({
  selector: '.swiper-item'
});

/* Tabs */
let tabs_btn = $('.tabs__item_btn');
let tab = $('.tab');
$(tabs_btn).click(function(){
  let tab_id = $(this).attr('data-tab');

  $(tabs_btn).removeClass('current');
  $(tab).removeClass('current');

  $(this).addClass('current');
  $("#"+tab_id).addClass('current');
});
/* Autors */

$('.painter__link').click( function getArticleList() {
  let searchFor = $(this).text();

  $.getJSON( "./js/data.json", function(json) {

      let picCard = $('.card__pic');
      let nameCard = $('.card__title');
      let dateCard = $('.card__lifetime');
      let descrCard = $('.card__descr');
      let picAutor = json.filter(autor => autor.name === searchFor).map(p => p.picture)[0];
      let nameAutor = json.filter(autor => autor.name === searchFor).map(n => n.name)[0];
      let dateOfBirdthAutor = json.filter(autor => autor.name === searchFor).map(db => db.dateOfBirdth)[0];
      let dateOfDeathAutor = json.filter(autor => autor.name === searchFor).map(dd => dd.dateOfDeath)[0];
      let descrAutor = json.filter(autor => autor.name === searchFor).map(d => d.descr)[0];
      picCard.attr('data-iesrc', './img/catalog/cards/' + picAutor + '.jpg');
      picCard.html("<source srcset='./img/catalog/cards/" + picAutor + ".jpg, ./img/catalog/cards/" + picAutor + "-retina.jpg 2x'> <img src='./img/catalog/cards/" + picAutor + ".jpg' alt='" + searchFor +"'>");
      nameCard.text(searchFor);
      dateCard.text(dateOfBirdthAutor + ' - ' + dateOfDeathAutor);
      descrCard.text(descrAutor);
  });
  let windowWidth = $(window).width();
  if (windowWidth <= 840) {
    $('html, body').stop().animate({
      scrollTop: $('.tab.current .tab__container > .card').offset().top - 60
    }, 800);
  };

});

/* Accordion */
let acc_btn = $(".accordion__item_btn")
let acc_descr = $('.accordion__item_descr')
$(acc_btn).on("click", function() {
  if ($(this).hasClass("active")) {

    $('.accordion__item').removeClass('active');
    $('.accordion__item_icon').removeClass('active');
    $(this).removeClass('active');
    $(this).stop(true, true)
      .siblings(acc_descr)
      .slideUp(400);
  } else {

    $('.accordion__item').removeClass('active');
    $(acc_btn).removeClass('active');
    $('.accordion__item_icon').removeClass('active');
    $(this).addClass('active');
    $(this).parents('.accordion__item').addClass('active');
    $(this).children('.accordion__item_icon').addClass('active');
    $(acc_descr).slideUp(400);
    $(this)
      .siblings(acc_descr)
      .slideDown(400);
  };
});

/* Events */
$('.events__more-btn').on('click', function () {
  $(this).hide();
  $('.event').addClass('show');
});



let eventsSwiper = null;
let editionsSwiper = null;
let mediaQuerySize = 575;

function eventsSwiperInit() {
  if (!eventsSwiper) {
    eventsSwiper = new Swiper('.events__swiper', {
      slidesPerView: 1,
      slidesPerColumn: 1,
      spaceBetween: 50,
      loop: true,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
      },
    });
  };
};
function eventsSwiperDestroy() {
  if (eventsSwiper) {
    eventsSwiper.destroy();
    eventsSwiper = null;
  };
};
/* Editions */
function editionsSwiperInit() {
  if (!editionsSwiper) {
    editionsSwiper = new Swiper('.editions__swiper', {
      slidesPerView: 2,
      slidesPerGroup: 2,
      spaceBetween: 34,
      breakpoints: {
        840: {
          slidesPerView: 2,
          slidesPerGroup: 2,
          spaceBetween: 50,
        },
        1360: {
          slidesPerView: 3,
          slidesPerGroup: 3,
          spaceBetween: 50,
        }
      },
      pagination: {
        el: '.editions__carousel .swiper-pagination',
        type: 'custom',
        renderCustom: function (swiper, current, total) {
          return current + ' / ' + total;
        },
      },
      navigation: {
        nextEl: '.editions__carousel .swiper-button-next',
        prevEl: '.editions__carousel .swiper-button-prev',
      }
    });
  };
};
function editionsSwiperDestroy() {
  if (editionsSwiper) {
    editionsSwiper.destroy();
    editionsSwiper = null;
  };
};

$( window ).on( "orientationchange", function (e ) {
  if(window.orientation == 0) {
  $('.categories__container').attr('open', false);
  }
});


$(window).on('load resize', function () {
  // Берём текущую ширину экрана

  let windowWidth = $(window).width();
  // Если ширина экрана меньше или равна mediaQuerySize(1024)
  if (windowWidth <= 575) {
    // Инициализировать слайдер если он ещё не был инициализирован
    $('.events__container').addClass('swiper-wrapper');
    $('.event').addClass('swiper-slide');
    eventsSwiperInit();
  } else {
    // Уничтожить слайдер если он был инициализирован
    eventsSwiperDestroy();
    $('.events__container').removeClass('swiper-wrapper');
    $('.event').removeClass('swiper-slide');
  };

  if (windowWidth <= 475) {
    // Уничтожить слайдер если он был инициализирован
    editionsSwiperDestroy();
    $('.editions__swiper').removeClass('swiper-container');
    $('.editions__swiper_container').removeClass('swiper-wrapper');
    $('.product').removeClass('swiper-slide');

  } else {
    // Инициализировать слайдер если он ещё не был инициализирован
    $('.editions__swiper').addClass('swiper-container');
    $('.editions__swiper_container').addClass('swiper-wrapper');
    $('.product').addClass('swiper-slide');
    editionsSwiperInit();

    /* Editions */
    $('.categories__container').attr('open', true);
  }
});
/* Editions */

const check_btn = $('.categories__list.select .categories__item > input');
const selected_btn = $('.categories__list.selected .categories__item > input');
$(check_btn).click(function (event){

  const check_id =  $(this).attr('name');
  let windowWidth = $(window).width();

  if (windowWidth <= 475) {
    $('.categories__list.selected .categories__item > input[name=' + check_id + ']').prop('checked', true);
    $('.categories__list.selected .categories__item > input[name=' + check_id + ']').parents('.categories__item').css('display', 'flex');
    $(this).parents('.categories__item').css('display', 'none');
  }
});
$(selected_btn).click(function (event){
  const selected_id =  $(this).attr('name');
  let windowWidth = $(window).width();

  if (windowWidth <= 475) {
    $(this).prop('checked', false);
    $('.categories__list.select .categories__item > input[name=' + selected_id + ']').prop('checked', false);
    $('.categories__list.select .categories__item > input[name=' + selected_id + ']').parents('.categories__item').css('display', 'flex');
    $(this).parents('.categories__item').css('display', 'none');
  }
});

const editionsBtn = $('.categories')

$(document).click(function (e) {
  let windowWidth = $(window).width();
  if (windowWidth <= 475) {
    if (!editionsBtn.is(e.target) && editionsBtn.has(e.target).length === 0) {
      editionsBtn.attr('open', false);
    };
  };
});
/* Projects tooltip */
tippy('#tooltip-1', {
  maxWidth: 264,
  content: 'Пример современных тенденций - современная методология разработки',
});

tippy('#tooltip-2', {
  maxWidth: 264,
  content: 'Приятно, граждане, наблюдать, как сделанные на базе аналитики выводы вызывают у вас эмоции',
});

tippy('#tooltip-3', {
  maxWidth: 264,
  content: 'В стремлении повысить качество',
});

/* Projects swiper */
const projectsSwiper = new Swiper('.projects__swiper', {
  slidesPerView: 1,
  spaceBetween: 21,
  loop: false,
  breakpoints: {
    475: {
      slidesPerView: 2,
      slidesPerGroup: 2,
      spaceBetween: 34,
    },
    840: {
      slidesPerView: 2,
      slidesPerGroup: 2,
      spaceBetween: 50,
    },
    1360: {
      slidesPerView: 3,
      slidesPerGroup: 3,
      spaceBetween: 50,
    },
  },
  navigation: {
    nextEl: '.projects__carousel .swiper-button-next',
    prevEl: '.projects__carousel .swiper-button-prev',
  },
});
/* Contacts */

/* Contacts map*/
ymaps.ready(init);
  function init(){
    let myPlacemark = new ymaps.Placemark([55.758463, 37.601079], {}, {
      iconLayout: 'default#image',
      iconImageHref: '../img/contacts/map-circle.svg',
      iconImageSize: [28, 40],
      iconImageOffset: [-14, -50]
    });

    let myMap = new ymaps.Map("map", {
        center: [55.762157, 37.618639],
        zoom: 13,
        controls: []
    },
    {
      suppressMapOpenBlock: true
  });
    myMap.geoObjects.add(myPlacemark);
  }
/* Form validate */
let selector = document.querySelector("input[type='tel']")
let im = new Inputmask("+7 (999) 999-99-99");

im.mask(selector);

new JustValidate('#subscribe', {
  colorWrong: '#FF5C00',
  rules: {
    name: {
      required:true,
      minLength: 2,
      maxLength: 15,
    },
    tel: {
      required: true,
      function: (name, value) => {
        const phone = selector.inputmask.unmaskedvalue()
        return Number(phone) && phone.length === 10
      }
    },
    email: {
      required: true,
      email: true
    },
  },
  messages: {
    name: {
      required: 'Как вас зовут?',
      minLength: 'Имя не может быть менее 2 символов'
    },
    tel: 'Укажите ваш телефон',
    email: 'Укажите ваш e-mail',
  },
  submitHandler: function (form, values, ajax) {

    ajax({
      url: '../send.php',
      method: 'POST',
      data: values,
      async: true,
      callback: function(response)  {
        console.log(response)
      }
    });
  },
});
